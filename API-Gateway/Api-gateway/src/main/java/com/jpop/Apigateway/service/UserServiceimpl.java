package com.jpop.Apigateway.service;

import com.jpop.Apigateway.Repository.UserRepository;
import com.jpop.Apigateway.entity.UserCredentialsDAO;
import com.jpop.Apigateway.entity.UserCredentialsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service(value = "userService")
public class UserServiceimpl implements UserService, UserDetailsService {
    @Autowired
    private UserRepository userDao;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserCredentialsDAO save(UserCredentialsDTO user) {
        UserCredentialsDAO newuser = new UserCredentialsDAO();
        newuser.setUsername(user.getUsername());
        newuser.setPassword(passwordEncoder.encode(user.getPassword()));
        return userDao.save(newuser);
    }

    @Override
    public UserCredentialsDAO findOne(String username) {
        return userDao.findByUsername(username);
    }

    public UserDetails loadUserByUsername(String userId) throws
            UsernameNotFoundException {
        UserCredentialsDAO user = userDao.findByUsername(userId);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new User(
                user.getUsername(), user.getPassword(), new ArrayList<>());
    }
}
