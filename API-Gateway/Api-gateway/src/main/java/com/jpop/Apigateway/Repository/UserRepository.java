package com.jpop.Apigateway.Repository;

import com.jpop.Apigateway.entity.UserCredentialsDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UserRepository extends JpaRepository<UserCredentialsDAO,Long> {
    UserCredentialsDAO findByUsername(String username);
}
