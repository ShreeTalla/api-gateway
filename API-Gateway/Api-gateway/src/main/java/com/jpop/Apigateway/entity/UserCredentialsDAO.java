package com.jpop.Apigateway.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name ="credentials")
public class UserCredentialsDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;

    private String password;

    public UserCredentialsDAO() {
    }

    public UserCredentialsDAO(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

}
