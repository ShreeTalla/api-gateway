package com.training.APIGateway.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.stereotype.Component;

@Component
public class RouteFilters extends ZuulFilter {
    public static final int FILTER_ORDER=1;
    public static final String FILTER_TYPE="route";
    public static final boolean SHOULD_FILTER=true;

    @Override
    public String filterType() {
        return FILTER_TYPE;
    }

    @Override
    public int filterOrder() {
        return FILTER_ORDER;
    }

    @Override
    public boolean shouldFilter() {
        return SHOULD_FILTER;
    }

    @Override
    public Object run() throws ZuulException {
        System.out.println("inside route filter");
        return null;
    }
}
